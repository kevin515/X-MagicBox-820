module(...,package.seeall)

local timer_id

--串口对应列表
local usbList = {uart.USB,1,}
local usbShow = {"USB","1",}
--波特率
local baud = {1200,2400,4800,9600,10400,14400,19200,28800,38400,57600,115200,230400,460800,921600}
--校验位
local parity = {uart.PAR_NONE,uart.PAR_EVEN,uart.PAR_ODD}
local parityShow = {"无校验","奇校验","偶校验"}
--停止位
local stop = {uart.STOP_1,uart.STOP_2}
local stopShow = {"1","2"}
--当前选项
local now = 1

--缓存串口数据
local uartTemp = {}
local MAX_COL,MAX_ROW = (240-56)/lcd.CHAR_WIDTH,240/lcd.CHAR_WIDTH/2-2
local function cropTemp() while #uartTemp > MAX_ROW do table.remove(uartTemp,1) end end
local function addLine(s,send)
    if list[6].get() then
        s = s:toHex()
    else
        s = s:gsub("\r","")
    end
    if list[7].get() then
        local t = os.date("*t")
        s = string.format("[%02d:%02d:%02d]",t.hour,t.min,t.sec)..s
    end
    s = s:split("\n")
    for i=1,#s do
        --if s[i]:len() > 0 or i ~= #s then--防止行末尾是回车换行
            while true do--截断字符串直到小于一行
                cropTemp()
                if s[i]:len() > MAX_COL then
                    table.insert(uartTemp,{s[i]:sub(1,MAX_COL),send})
                    s[i] = s[i]:sub(MAX_COL+1)
                else
                    table.insert(uartTemp,{s[i],send})
                    break
                end
            end
        --end
    end
    cropTemp()
    page.update()
end


--串口读缓冲区
local sendQueue = {}
sys.subscribe("UART_RECEIVED", function ()
    local s = table.concat(sendQueue)
    -- 串口的数据读完后清空缓冲区
    sendQueue = {}
    addLine(s)
end)

--更改串口配置
local function changeUart()
    for i=1,#usbList do uart.close(usbList[i]) end
    if list[1].get() then
        local id = list[2].get()
        uart.on(id, "receive", function(uid, length)
            table.insert(sendQueue, uart.read(id, 9999))
            sys.timerStart(sys.publish, 50, "UART_RECEIVED")
        end)
        uart.setup(id,list[3].get(),8,list[4].get(),list[5].get())
        if id == 1 then
            uart.set_rs485_oe(id,19)
        end
    end
end

list = {
    {--2
        name = "启用",
        get = function() return list[1].private end,
        set = function (add)
            list[1].private = not list[1].private
            changeUart()
        end,
        show = function() return list[1].private and "是" or "否" end,
        private = nil,
    },
    {--2
        name = "串口",
        get = function() return usbList[list[2].private] end,
        set = function (add)
            list[2].private = tools.loopAdd(list[2].private + add,#usbList,1)
            changeUart()
        end,
        show = function() return usbShow[list[2].private] end,
        private = 1,
    },
    {--3
        name = "波特率",
        get = function() return baud[list[3].private] end,
        set = function (add)
            list[3].private = tools.loopAdd(list[3].private + add,#baud,1)
            changeUart()
        end,
        show = function() return tostring(baud[list[3].private]) end,
        private = 4,
    },
    {--4
        name = "校验位",
        get = function() return parity[list[4].private] end,
        set = function (add)
            list[4].private = tools.loopAdd(list[4].private + add,#parity,1)
            changeUart()
        end,
        show = function() return parityShow[list[4].private] end,
        private = 1,
    },
    {--5
        name = "停止位",
        get = function() return stop[list[5].private] end,
        set = function (add)
            list[5].private = tools.loopAdd(list[5].private + add,#stop,1)
            changeUart()
        end,
        show = function() return stopShow[list[5].private] end,
        private = 1,
    },
    {--6
        name = "显示HEX",
        get = function() return list[6].private end,
        set = function (add)
            list[6].private = not list[6].private
        end,
        show = function() return list[6].private and "是" or "否" end,
        private = nil,
    },
    {--7
        name = "时间",
        get = function() return list[7].private end,
        set = function (add)
            list[7].private = not list[7].private
        end,
        show = function() return list[7].private and "开" or "关" end,
        private = true,
    },
}


function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("串口测试",120,lcd.gety(0),255,255,255)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    for i=1,#list do
        if now == i then
            disp.drawrect(0,lcd.gety(i*2-2),55,lcd.gety(i*2)-1,lcd.rgb(50,50,50))
        end
        lcd.putStringCenter(list[i].name,27,lcd.gety(i*2-2),255,255,255)
        lcd.putStringCenter(list[i].show(),27,lcd.gety(i*2-1),200,50,50)
    end
    disp.drawrect(56,lcd.gety(2),239,239,lcd.rgb(25,25,25))
    for i=1,#uartTemp do
        if uartTemp[i][2] then
            disp.setcolor(lcd.rgb(100,100,255))
            disp.puttext(uartTemp[i][1],56,lcd.gety(i+1))
        else
            disp.setcolor(lcd.rgb(0,255,0))
            disp.puttext(uartTemp[i][1],56,lcd.gety(i+1))
        end
    end
end

local keyEvents = {
    A = function ()
        if list[1].get() then
            local s = tostring(os.time())
            uart.write(list[2].get(),s)
            addLine(s,true)
        end
    end,
    UP = function ()
        now=tools.loopAdd(now-1,#list,1)
    end,
    DOWN = function ()
        now=tools.loopAdd(now+1,#list,1)
    end,
    LEFT = function ()
        list[now].set(-1)
    end,
    RIGHT = function ()
        list[now].set(1)
    end,
}

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    --timer_id = sys.timerLoopStart(page.update,100)
end

function close()
    -- if timer_id then
    --     sys.timerStop(timer_id)
    --     timer_id = nil
    -- end
end

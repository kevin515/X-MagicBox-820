module(...,package.seeall)

--当前的灯状态
local R,G,B

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("RGB灯测试",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("警告",120,lcd.gety(3),255,0,0)
    lcd.putStringCenter("灯亮刺眼",120,lcd.gety(5),255,0,0)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.putStringCenter("上方第3至第5键控制三色开关",120,lcd.gety(3),100,100,100)
end


local keyEvents = {
    ["5"] = function ()
        R = not R
        pmd.ldoset(R and 7 or 0,pmd.LDO_VBACKLIGHT_G)
    end,
    ["3"] = function ()
        G = not G
        pmd.ldoset(G and 7 or 0,pmd.LDO_VBACKLIGHT_B)
    end,
    ["4"] = function ()
        B = not B
        pmd.ldoset(B and 7 or 0,pmd.LDO_VBACKLIGHT_W)
    end,
}
function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end


module(...,package.seeall)
require"start"
require"logShow"
require"settings"
require"light"
require"ckey"
require"map"
require"uartShow"
require"mqttShow"
require"tfCard"
require"bird"
require"qqMessage"


--当前页码
local now = 1
--页面列表
local pages = {
    "start",
    "map",
    "qqMessage",
    "uartShow",
    "mqttShow",
    "tfCard",
    "light",
    "ckey",
    "bird",
    "settings",
    "logShow",
}

--刷新页面
function update()
    if _G[pages[now]].update then
        _G[pages[now]].update()
        if not lcd.issdisp then disp.update() end
    end
end


--切换页面
function change(index)
    if index then
        --audio.play(0, "FILE", "/lua/t.mp3",nvm.get("vol"))
        if _G[pages[now]].close then _G[pages[now]].close() end
        now = index
    end
    if _G[pages[now]].open then _G[pages[now]].open() end
    update()
end





sys.subscribe("START_DONE", function ()
    change()
    --订阅按键事件
    sys.subscribe("KEY_EVENT", function(k,e)
        if k == "1" and e then
            local p = now - 1
            if p < 1 then p = #pages end
            change(p)
        elseif k == "2" and e then
            local p = now + 1
            if p > #pages then p = 1 end
            change(p)
        else
            if _G[pages[now]].key then _G[pages[now]].key(k,e) end
        end
    end)
end)

--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "TEST"
VERSION = "1.0.1"

--根据固件判断模块类型
moduleType = string.find(rtos.get_version(),"8955") and 2 or 4

require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "misc"
require "net"
require"pm"

require"nvm"
require"config"
nvm.init("config.lua")
require"utils"

require"audio"
audio.setStrategy(1)
--audio.play(0, "FILE", "/lua/xx.mp3",nvm.get("vol"))

require"tools"
require"lcd"
require"st7789"
require"keycfg"

gps = require"gpsZkw"
--agps功能模块只能配合Air800或者Air530使用；如果不是这两款模块，不要打开agps功能
--require"agpsZkw"

require"ntp"
ntp.timeSync()
--require"bmp280"

require"page"
--require"gpsTest"

--启动系统框架
sys.init(0, 0)
sys.run()

module(...,package.seeall)

local timer_id
--当前的设置项
local now = 1

local list = {
    {
        name = "飞行模式",
        get = function()
            return net.flyMode and "开" or "关"
        end,
        set = function (add)
            net.switchFly(not net.flyMode)
        end
    },
    {
        name = "GPS定位",
        get = function()
            return gps.isOpen() and "开" or "关"
        end,
        set = function (add)
            require"agpsZkw"
            if gps.isOpen() then
                gps.closeAll()
            else
                gps.setNmeaMode(2,function(msg) log.info("NMEA",msg) end)
                gps.open(gps.DEFAULT,{tag="GPS",cb=test1Cb})
            end
        end
    },
    {
        name = "音量",
        get = function()
            return nvm.get("vol")
        end,
        set = function (add)
            local vol = nvm.get("vol") + add
            if vol < 0 then vol = 0 end
            if vol > 7 then vol = 7 end
            nvm.set("vol",vol)
            audio.setVolume(vol)
        end
    },
}

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("功能开关",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter(list[now].name,120,lcd.gety(3),255,50,50)
    lcd.putStringCenter("<"..list[now].get()..">",120,lcd.gety(5),50,50,255)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8
    lcd.putStringCenter("上下切换项目，左右更改设置",120,lcd.gety(3),100,100,100)
end


local keyEvents = {
    UP = function ()
        now = now - 1
        if now < 1 then now = #list end
    end,
    DOWN = function ()
        now = now + 1
        if now > #list then now = 1 end
    end,
    LEFT = function ()
        list[now].set(-1)
    end,
    RIGHT = function ()
        list[now].set(1)
    end,
}
function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    timer_id = sys.timerLoopStart(page.update,1000)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end

module(...,package.seeall)
require"mqtt"
local timer_id

--开启mqtt
local enable

--缓存mqtt数据
local mqttTemp = {}
local MAX_COL,MAX_ROW = 240/lcd.CHAR_WIDTH,240/lcd.CHAR_WIDTH/2-2
local function cropTemp() while #mqttTemp > MAX_ROW do table.remove(mqttTemp,1) end end
local function addLine(h,msg)
    table.insert(mqttTemp,{common.utf8ToGb2312(h),lcd.rgb(0,255,0)})
    local s = common.utf8ToGb2312(msg):gsub("\r",""):split("\n")
    for i=1,#s do
        while true do--截断字符串直到小于一行
            cropTemp()
            if s[i]:len() > MAX_COL then
                table.insert(mqttTemp,{s[i]:sub(1,MAX_COL),lcd.rgb(100,100,255)})
                s[i] = s[i]:sub(MAX_COL+1)
            else
                table.insert(mqttTemp,{s[i],lcd.rgb(100,100,255)})
                break
            end
        end
    end
    cropTemp()
    page.update()
end


function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("QQ群消息",120,lcd.gety(0),255,255,255)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.text("<",lcd.getx(24),lcd.gety(0),255,255,255)
    lcd.text(enable and "开启" or "关闭",lcd.getx(25),lcd.gety(0),100,100,255)
    lcd.text(">",lcd.getx(29),lcd.gety(0),255,255,255)
    lcd.text("左右开关",lcd.getx(23),lcd.gety(1),100,100,100)

    disp.drawrect(0,lcd.gety(2),239,239,lcd.rgb(25,25,25))
    for i=1,#mqttTemp do
        disp.setcolor(mqttTemp[i][2])
        disp.puttext(mqttTemp[i][1],0,lcd.gety(i+1))
    end
end

local keyEvents = {
    -- A = function ()
    --     if enable then sys.publish("MQTT_SEND_DATA_QQ",tostring(os.time())) end
    -- end,
    LEFT = function ()
        enable = not enable
        if not enable then sys.publish("MQTT_SEND_DATA_IND_QQ") end
    end,
    RIGHT = function ()
        enable = not enable
        if not enable then sys.publish("MQTT_SEND_DATA_IND_QQ") end
    end,
}

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    --timer_id = sys.timerLoopStart(page.update,100)
end

function close()
    -- if timer_id then
    --     sys.timerStop(timer_id)
    --     timer_id = nil
    -- end
end

--需要订阅的主题
local subscribeTopics
--请按需求来更改订阅的主题内容
sys.subscribe("IMEI_READY_IND",function ()--确保imei可被读到
    subscribeTopics = {
        ["luaRobot/message/+"] = 0,--qos为0
    }
end)
--待发送数据缓冲区
local toSend = {}
-- sys.subscribe("MQTT_SEND_DATA_QQ",function (data)
--     table.insert(toSend,{
--         topic = (misc.getImei() or "").."/upload",
--         payload = data
--     })
--     sys.publish("MQTT_SEND_DATA_IND_QQ")
-- end)
sys.taskInit(function()
    local r, data
    while true do
        while not socket.isReady() do sys.wait(1000) end
        local mqttc = mqtt.client(misc.getImei(), 300, "user", "password")--心跳时间300秒，用户名密码为默认
        while not enable do sys.wait(1000) end--没开启mqtt
        while not mqttc:connect("broker.emqx.io", 1883) do sys.wait(2000) end
        if mqttc:subscribe(subscribeTopics) then--订阅主题
            sys.publish("MQTT_CONNECTED_QQ")
            while true do
                r, data = mqttc:receive(120000, "MQTT_SEND_DATA_IND_QQ")
                if r then
                    log.info("mqtt receive", data.topic,data.payload)
                    sys.publish("MQTT_RECEIVE_QQ",data)
                elseif data == "MQTT_SEND_DATA_IND_QQ" then
                    if not enable then break end--强制断开mqtt连接
                    local ar
                    while #toSend > 0 do
                        local mdata = table.remove(toSend,1)
                        log.info("mqtt send",mdata.topic,mdata.payload)
                        local mr = mqttc:publish(mdata.topic, mdata.payload)
                        if not mr then ar = true break end
                    end
                    if ar then break end
                elseif data == "timeout" then
                    --没必要处理这东西
                else
                    break
                end
            end
        end
        sys.publish("MQTT_DISCONNECTED_QQ")
        r, data = nil,nil
        mqttc:disconnect()
    end
end)

--mqtt服务器连接上之后调用的代码
sys.subscribe("MQTT_CONNECTED_QQ",function ()
    addLine("服务器已连接","连接状态")
    log.info("mqttTask", "server connected!")
end)

--mqtt服务器断开之后调用的代码
sys.subscribe("MQTT_DISCONNECTED_QQ",function ()
    addLine("服务器已断开","连接状态")
    log.info("mqttTask", "server disconnected!")
end)

-- 收到mqtt数据的处理
sys.subscribe("MQTT_RECEIVE_QQ",function (params)
    local j,r,e = json.decode(params.payload)
    if not r then log.warn("qq json decode error", e) end
    addLine(string.format("[%s][%s]",j.name,j.qq),j.msg)
end)

module(...,package.seeall)

--当前选中
local now = 1
--最大行数
local lineMax = 12

--是否要显示其他东西
local showOther

local files = {}
local sdCardFreeSize,sdCardTotalSize = 0,0

local function getSpace()
    files = {}
    if io.opendir("/sdcard0") then
        sdCardFreeSize = rtos.get_fs_free_size(1,1)
        sdCardTotalSize = rtos.get_fs_total_size(1,1)
        for i=1,100 do
            local fType,fName,fSize = io.readdir()
            if fType==32 then
                table.insert(files,{name=fName,size=fSize})
            elseif fType == nil then
                break
            end
        end
        io.closedir("/sdcard0")
    end
end

function update()
    if showOther then
        showOther = nil
        return
    end
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("TF卡",120,lcd.gety(0),255,255,255)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8
    if sdCardTotalSize > 0 then
        lcd.putStringCenter("可用/总空间:"..sdCardFreeSize.."/"..sdCardTotalSize,
                            120,lcd.gety(2),200,200,200)
        local min,max = 1,#files
        if #files > lineMax then
            if #files - now < lineMax - 1 then
                min = max - lineMax + 1
            else
                min = now
                max = now + lineMax - 1
            end
        end
        for i=min,max do
            local where = i-min+1
            if now == i then
                lcd.text(">",0,lcd.gety(where+2),100,255,100)
            end
            lcd.text(files[i].name,8,lcd.gety(where+2),100,255,100)
            lcd.putStringRight(tostring(files[i].size).."B",240,lcd.gety(where+2),100,255,100)
        end
    else
        lcd.putStringCenter("未检测到卡,请插入后重启再试",120,lcd.gety(6),255,255,255)
    end
end


local keyEvents = {
    A = function ()
        local subname = files[now].name:sub(-4,-1):upper()
        if subname == ".MP3" or subname == ".PCM" or subname == ".WAV" then
            audio.play(0, "FILE", "/sdcard0/"..files[now].name, nvm.get("vol"))
        elseif subname == ".JPG" or subname == ".PNG" or subname == ".BMP" then
            showOther = true
            disp.putimage("/sdcard0".."/"..files[now].name,0,0,-1,0,0,239,239)
        elseif subname == ".LUA" then
            showOther = true
            xpcall(function()
                local f = loadfile("/sdcard0/"..files[now].name)
                if f then f() end
            end,
            function(err) -- 错误输出
                log.info("lua",err)
                log.info("lua",debug.traceback())
            end)
        end
    end,
    B = function ()
        audio.stop()
    end,
    UP = function ()
        now=tools.loopAdd(now-1,#files,1)
    end,
    DOWN = function ()
        now=tools.loopAdd(now+1,#files,1)
    end,
    LEFT = function ()
        now=tools.lineAdd(now-lineMax,#files,1)
    end,
    RIGHT = function ()
        now=tools.lineAdd(now+lineMax,#files,1)
    end,
}
function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end
function open()
    getSpace()
end
function close()
    now = 1
    showPic = nil
    files = nil
end

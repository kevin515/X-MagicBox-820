module(...,package.seeall)
require"http"
--[[
012345678901234567890123456789|
       魔盒超级调试工具       |
 按底板左上两个键可以切换页面 |
          底层版本        4000|
    2000-00-00 00:00:00       |
867258051330641 联网:NO CSQ:31|
89860446101980203880 RSRP:-140|
GPS:开 已定位:否 可见:99 用:99|
123.1234567 E   123.1234567 N |
  218.820.175.207 上海 移动   |
]]

local timer_id
local ip = "ip获取中..."
local ip_r = ""
local poem = ""
local netMode = {
[0]="NO",
[1]="2G",
[2]="2.5G",
[3]="3G",
[4]="4G",
[5]="3G",
}

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("魔盒助手",120,0,255,255,255)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.putStringCenter("按底板左上两个键可以切换页面",120,lcd.gety(2),100,100,100)
    lcd.putStringCenter(rtos.get_version() or "获取版本失败",120,lcd.gety(3),50,50,255)

    lcd.putStringRight((misc.getVbatt() or 0).."mV",240,lcd.gety(4),50,150,50)

    local t = os.date("*t")
    lcd.putStringCenter(string.format("%04d-%02d-%02d %02d:%02d:%02d",
        t.year,t.month,t.day,
        t.hour,t.min,t.sec),
        110,lcd.gety(4),0,255,50)

    lcd.text(misc.getImei() or "获取中...",0,lcd.gety(6),50,255,255)
    if net.flyMode then
        lcd.text("飞行模式",lcd.getx(16),lcd.gety(6),255,50,50)
    else
        lcd.text("联网:",lcd.getx(16),lcd.gety(6),255,50,255)
        lcd.text(tostring(netMode[net.getNetMode()]),lcd.getx(21),lcd.gety(6),255,50,255)
        lcd.text("CSQ:",lcd.getx(24),lcd.gety(6),255,50,255)
        lcd.text(tostring(net.getRssi()),lcd.getx(28),lcd.gety(6),255,50,255)
        lcd.text("RSRP:",lcd.getx(21),lcd.gety(7),255,50,255)
        lcd.text(tostring(net.getRsrp()),lcd.getx(26),lcd.gety(7),255,50,255)
    end
    if sim.getStatus() then
        lcd.text(sim.getImsi() or "获取中...",lcd.getx(0),lcd.gety(7),50,255,255)
    else
        lcd.text("未检测到卡",lcd.getx(0),lcd.gety(7),50,255,255)
    end


    if gps.isOpen() then
        lcd.text("GPS:开 "..(gps.isFix() and "已定位" or "未定位")..
        " 可见:"..(gps.getViewedSateCnt() or 0)..
        " 可用:"..(gps.getUsedSateCnt() or 0)
        ,lcd.getx(0),lcd.gety(9),100,255,0)
        local loc = gps.getLocation()
        lcd.text(loc.lng.." "..loc.lngType,lcd.getx(0),lcd.gety(10),100,255,0)
        lcd.putStringRight(loc.lat.." "..loc.latType,239,lcd.gety(10),100,255,0)
    else
        lcd.putStringCenter("GPS功能没开",120,lcd.gety(9),100,255,0)
        lcd.putStringCenter("未定位",120,lcd.gety(10),100,255,0)
    end

    lcd.putStringCenter(ip,120,lcd.gety(12),255,0,100)
    lcd.putStringCenter(ip_r,120,lcd.gety(13),255,0,100)
    lcd.putStringCenter(poem,120,lcd.gety(14),50,255,100)
end

function open()
    timer_id = sys.timerLoopStart(page.update,100)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end

http.request("GET","https://ip.fm/",nil,{["user-agent"]="curl"},nil,120000,function (result,prompt,head,body)
    log.info("ip",result,prompt,head,body)
    if result then
        local ip_s,addr_s = body:match("IP: *(%d+%.%d+%.%d+%.%d+) *来自: *(.+)")
        ip = ip_s or "未知ip"
        ip_r = addr_s or "未知地址"
    else
        ip = "外网IP获取失败"
    end
end)

http.request("GET","https://v1.hitokoto.cn/?c=i&encode=text&charset=utf-8&max_length=16",nil,nil,nil,120000,function (result,prompt,head,body)
    if result then
        poem = body
    end
end)

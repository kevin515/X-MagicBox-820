module(...,package.seeall)
require"http"

local timer_id
--当前的设置项
local now = 0
--最大图片数量
local max = 21
--加载中的列表
local loading = {}

--是否插卡？
local tfCard

function update()
    disp.clear()
    if now == 0 then
        disp.setfontheight(32)
        lcd.CHAR_WIDTH = 16
        lcd.putStringCenter("地图",120,lcd.gety(0),255,255,255)
        lcd.putStringCenter("卫星地图",120,lcd.gety(3),255,50,50)
        lcd.putStringCenter(nvm.get("map") and "开" or "关",120,lcd.gety(5),50,50,255)
        disp.setfontheight(16)
        lcd.CHAR_WIDTH = 8
        lcd.putStringCenter("方向键切换图片",120,lcd.gety(3),100,100,100)
        lcd.putStringCenter("地图界面按A键刷新图片",120,lcd.gety(4),100,100,100)
        if tfCard then
            lcd.putStringCenter("tf卡已插入,图片会额外备份进卡",120,lcd.gety(14),100,100,100)
        else
            lcd.putStringCenter("开机前插入tf卡，可保存获取的图片",120,lcd.gety(14),100,100,100)
        end
    else
        local map = nvm.get("maps")[now]
        if map and now ~= max then
            disp.putimage("/maps".."/"..now..".jpg",0,0)
            disp.drawrect(0,224,151,239,0)
            local t = os.date("*t",map.ts)
            lcd.text(string.format("%04d-%02d-%02d %02d:%02d:%02d",
                t.year,t.month,t.day,
                t.hour,t.min,t.sec),
                0,lcd.gety(14),255,255,255)
                disp.drawrect(0,0,239,16,0)
                lcd.text(map.lng..","..map.lat,0,lcd.gety(0),255,255,255)
                lcd.putStringRight("图"..now,239,lcd.gety(0),255,255,255)
        elseif map and now == max then
            disp.putimage("/maps".."/"..now..".jpg",0,0)
            disp.drawrect(0,224,151,239,0)
            local t = os.date("*t")
            lcd.text(string.format("%04d-%02d-%02d %02d:%02d:%02d",
                t.year,t.month,t.day,
                t.hour,t.min,t.sec),
                0,lcd.gety(14),255,255,255)
                disp.drawrect(175,0,239,16,0)
                lcd.putStringRight("历史路径",239,lcd.gety(0),255,255,255)
        elseif loading[now] then
            disp.setfontheight(32)
            lcd.CHAR_WIDTH = 16
            lcd.putStringCenter((now == max and "历史路径" or "图"..now).."获取中",120,lcd.gety(1),255,50,50)
            lcd.putStringCenter("加载中，请稍候",120,lcd.gety(4),50,255,50)
            disp.setfontheight(16)
            lcd.CHAR_WIDTH = 8
        else
            disp.setfontheight(32)
            lcd.CHAR_WIDTH = 16
            lcd.putStringCenter((now == max and "历史路径" or "图"..now).."为空",120,lcd.gety(1),255,50,50)
            if (gps.isFix() and socket.isReady()) or now == max then
                lcd.putStringCenter("按A键加载图片",120,lcd.gety(4),50,255,50)
            else
                lcd.putStringCenter("无网络或未定位",120,lcd.gety(4),255,50,50)
            end
            disp.setfontheight(16)
            lcd.CHAR_WIDTH = 8
        end
    end
end

--下载地图
function getMap()
    if now ~= max and (now == 0 or not gps.isFix() or not socket.isReady() or loading[now]) then return end
    local path = "/maps".."/"..now..".jpg"
    local maps = nvm.get("maps")
    if maps[now] then--删掉之前的图
        os.remove(path)
        maps[now] = nil
        nvm.set("maps",maps)
    end
    maps = nil
    loading[now] = true--获取中标记

    local ts = os.time()
    local mapGet = now
    local loc = gps.getLocation()
    log.info("download", path)
    local req
    if now == max then
        local locs = nvm.get("locs")
        if #locs <= 2 then loading[now] = nil return end
        req = (nvm.get("map") and "1" or "0")..",240,240,"..table.concat(locs,",")
    else
        req = loc.lng..","..loc.lat..","..(nvm.get("map") and "1" or "0")
    end
    http.request("POST","http://map.wvvwvw.com/",--移动卡没法直连，走中转绕一下
        nil,nil,req,30000,
        function (result,statusCode,head)
            if result then
                local maps = nvm.get("maps")
                if mapGet == max then
                    maps[mapGet] = true
                else
                    maps[mapGet] = {
                        ts = ts,
                        lat = loc.lat,
                        lng = loc.lng,
                    }
                end
                nvm.set("maps",maps)
                local f1 = io.open(path,"rb")
                if f1 then
                    local t = os.date("*t",map.ts)
                    t = string.format("%04d-%02d-%02d,%02d_%02d_%02d",
                        t.year,t.month,t.day,
                        t.hour,t.min,t.sec)
                    local f2 = io.open("/sdcard0/map_"..t..".jpg","wb")
                    if f2 then
                        local temp
                        while true do
                            temp = f1:read(300)
                            if temp then
                                f2:write(temp)
                            else
                                break
                            end
                        end
                        f2:close()
                    end
                    f1:close()
                end
            else
                os.remove(path)
            end
            loading[mapGet] = nil
        end,
    path)
end

local keyEvents = {
    UP = function ()
        now = now - 1
        if now < 0 then now = max end
    end,
    DOWN = function ()
        now = now + 1
        if now > max then now = 0 end
    end,
    LEFT = function ()
        now = now - 1
        if now < 0 then now = max end
    end,
    RIGHT = function ()
        now = now + 1
        if now > max then now = 0 end
    end,
    OK = function ()
        if now == 0 then
            nvm.set("map",not nvm.get("map"))
        else
            getMap()
        end
    end,
}
keyEvents["A"] = keyEvents.OK

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    timer_id = sys.timerLoopStart(page.update,500)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end

rtos.make_dir("/maps".."/")

sys.timerLoopStart(function()
    if not gps.isFix() then return end
    local loc = gps.getLocation()
    local locs = nvm.get("locs")
    table.insert(locs,loc.lng)
    table.insert(locs,loc.lat)
    while #locs > 300 do table.remove(locs,1) end
    nvm.set("locs",locs)
end,60000)


sys.timerStart(function ()
    --io.mount(io.SDCARD)
    if io.opendir("/sdcard0") then
        tfCard = true
        io.closedir("/sdcard0")
    end
end, 3000)


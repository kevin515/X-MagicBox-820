module(...,package.seeall)

local seed = tonumber(tostring(os.time()):reverse():sub(1, 7)) + rtos.tick()
function randomseed(val)
    seed = val
end
function random()
    local next = seed
    next = next * 1103515245
    next = next + 12345
    local result = (next / 65536) % 2048
    next = next * 1103515245
    next = next + 12345
    result = result * 2 ^ 10
    result = bit.bxor(result, (next / 65536) % 1024)
    next = next * 1103515245
    next = next + 12345
    result = result * 2 ^ 10
    result = bit.bxor(result, (next / 65536) % 1024)
    seed = next
    return result
end

function randomStr(l)
    local s = {}
    for i=1,l do
        table.insert(s,string.char(random()%94+33))
    end
    return table.concat(s)
end

function loopAdd(n,max,min)
    if n > max then return min end
    if n < min then return max end
    return n
end

function lineAdd(n,max,min)
    if n > max then return max end
    if n < min then return min end
    return n
end

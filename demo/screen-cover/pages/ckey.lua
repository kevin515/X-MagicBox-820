module(...,package.seeall)

local timer_id

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("电容按键测试",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("按键状态",120,lcd.gety(3),0,255,0)
    lcd.putStringCenter(keycfg.getC() and "按下" or "松开",120,lcd.gety(5),50,200,50)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8
end

function open()
    timer_id = sys.timerLoopStart(page.update,100)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end
